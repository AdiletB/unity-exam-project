using UnityEngine;

public class PlayerCollision:MonoBehaviour
{
    [SerializeField]
    private GameObject player;
    private GameController gameController;

    void Start(){
        gameController = player.GetComponent<GameController>();
    }

    private void OnCollisionEnter(Collision other) {
       if(other.collider.tag == "LandMine")
       {
           Destroy(other.gameObject);
           gameController.SetHealth(0);
           gameController.EndGame();
       }
    }
}