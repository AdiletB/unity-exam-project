﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class AmmoBoxCollision : MonoBehaviour
{    
    private void OnTriggerStay(Collider other) 
    {
        if (other.tag == "Player" && Input.GetKeyDown(KeyCode.E))
        {
            var gunScript = GameObject.Find("Heavy_08")
                                      .GetComponent<GunScript>();
            gunScript.Refill(10);
        }
            
    }
}
