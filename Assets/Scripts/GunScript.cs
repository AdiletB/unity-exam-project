﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class GunScript : MonoBehaviour
{
    [SerializeField]
    private int shootSpeed, damage;
    [SerializeField]
    private float range;
    [SerializeField]
    private AudioSource audioSource;
    [SerializeField]
    private ParticleSystem shootEffect;
    [SerializeField]
    private GameObject hitEffect;
    [SerializeField]
    private Camera playerCamera;
    private AmmoController ammoController;
    private GameController gameController;
    void Start()
    {
       ammoController = GetComponent<AmmoController>();
       gameController = playerCamera.GetComponent<GameController>();
    }
    void Update()
    {
        if(Input.GetButtonDown("Fire1")){
            Shoot();
        }
        if(Input.GetKeyDown("r"))
        {
            ammoController.Reload();
        }
    }
    private void Shoot()
    {
        if(ammoController.GetCurrentAmmo() <= 0)
        {
            Debug.Log("Empty ammo");
            return;
        }
        RaycastHit hit;
        if(Physics.Raycast(playerCamera.transform.position, playerCamera.transform.forward, out hit, range))
        {
            shootEffect.Play();
            GameObject effect = Instantiate(hitEffect, hit.point, Quaternion.LookRotation(hit.normal));
            Destroy(effect, 1f);

            Debug.Log("Hit" + hit.collider.tag);
            switch (hit.collider.tag)
            {
                case "Target": 
                    TargetHit(hit.collider.gameObject);
                break;

                default:
                break;
            }
            int currentAmmo = ammoController.GetCurrentAmmo();
            ammoController.SetCurrentAmmo(currentAmmo-1);
        }
    }
    public void Refill(int value)
    {
        Debug.Log("Refill");
        ammoController.RefillAmmo(value);
    }
    private void TargetHit(GameObject target)
    {
        var targetScript = target.GetComponent<ShooterTargetScript>();
        int currentTargetHealth = targetScript.GetHealth();
        Debug.Log("Current Health:" + currentTargetHealth);
        
        targetScript.SetHealth(currentTargetHealth -= damage);
        if(currentTargetHealth <= 0)
        {
            Destroy(target);
            gameController.TargetDestroyed();
            
            return;
        }
    }
}
