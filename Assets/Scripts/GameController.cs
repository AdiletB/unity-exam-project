using UnityEngine;
using UnityEngine.UI;
using UnityStandardAssets.Characters.FirstPerson;

public class GameController : MonoBehaviour
{
    [SerializeField]
    private GameObject player;
    [SerializeField]
    private Text healthText, gameOverText;
    [SerializeField]
    private int health;
    private ScoreController scoreController;
    private RigidbodyFirstPersonController playerController;

    void Start()
    {
        scoreController = GetComponent<ScoreController>();
        playerController = player.GetComponent<RigidbodyFirstPersonController>();

        SetHealth(health);
    }
    public void TargetDestroyed()
    {
        scoreController.AddScore(100);
    }
    public void SetHealth(int value)
    {
        health = value;
        healthText.text = "Health: " + health;
    }
    public void EndGame()
    {
        gameOverText.gameObject.SetActive(true);
        playerController.enabled = false;
    }
}