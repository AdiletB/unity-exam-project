using UnityEngine;
using UnityEngine.UI;

public class AmmoController: MonoBehaviour
{
    [SerializeField]
    private int maxAmmo;
    private int currentAmmo = 10;
    [SerializeField]
    private Text ammoText;
    void Start(){
        
        SetTextValue();
    }
    public int GetCurrentAmmo()
    {
        return currentAmmo;
    }
    public void SetCurrentAmmo(int value)
    {
        currentAmmo = value;
        SetTextValue();
    }
    public void RefillAmmo(int value)
    {
        if(maxAmmo + value > 30)
            return;
        
        this.maxAmmo += value;
        SetTextValue();
    }
    public void Reload()
    {
        if(currentAmmo == 10 || maxAmmo <= 0) return;

        currentAmmo = 0;
        currentAmmo += 10;
        maxAmmo -= 10;
        SetTextValue();

        Debug.Log($"Reload currAmmo: {currentAmmo}, maxAmmo: {maxAmmo}");
    }
    private void SetTextValue()
    {
        ammoText.text = $"{currentAmmo.ToString()} | {maxAmmo.ToString()}";
    }
}