using UnityEngine;
using UnityEngine.UI;


public class ScoreController : MonoBehaviour
{
    [SerializeField]
    private int scoreCount;
    [SerializeField]
    private Text scoreText;

    public void AddScore(int value)
    {
        scoreCount += value;
        scoreText.text = "Score: " + scoreCount.ToString();
    }
}