﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using UnityStandardAssets.Characters.FirstPerson;

public class InteractionController : MonoBehaviour
{
    [SerializeField]
    private RigidbodyFirstPersonController playerController;
    [SerializeField]
    private float interactDistance;
    [SerializeField]
    private LayerMask layerMask;
    [SerializeField]
    private GameObject gun;
    void Start()
    {
        
    }
    void Update()
    {
        Ray ray = new Ray (transform.position, transform.forward);
        RaycastHit raycastHit;
        if(Physics.Raycast(ray, out raycastHit, interactDistance, layerMask))
        {
            Debug.Log("1");
            if(Input.GetKeyDown(KeyCode.E))
            {
                Debug.Log("E");
                if(raycastHit.transform.tag == "Ammo")
                {
                    var script = gun.GetComponent<GunScript>();
                    script.Refill(10);
                    Debug.Log("Refill");
                }
                
            }
        }
    }
}
