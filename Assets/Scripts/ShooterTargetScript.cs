using UnityEngine;

public class ShooterTargetScript : MonoBehaviour
{
    [SerializeField]
    protected int healthCount;
    public void SetHealth(int value)
    {
        healthCount = value;
        Debug.Log("Target health:" + healthCount);
    }
    public int GetHealth()
    {
        return healthCount;
    }
}