﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class MovableTarget : ShooterTargetScript
{
    [SerializeField]
    private float speed, interval;
    private int tickCount = 0;
    private bool isNegate = false;
    private void Update() {
        if(healthCount < 0)
        {
            enabled = false;
        }
        HorizontalMovement();
        if(tickCount == interval/2)
        {
            isNegate = true;
                  
        }
        if(tickCount == interval)
        {
            tickCount = 0;
            isNegate = false;
        }
    }
    private void HorizontalMovement()
    {
        if(isNegate)
        {
            transform.Translate(new Vector3(-1, 0, 0) * Time.deltaTime * speed, Space.World);
        }
        else
        {
            transform.Translate(new Vector3(1, 0, 0) * Time.deltaTime * speed, Space.World);
        }  
        tickCount++;
    }
}
